class Solution:
    def dict_of_words(self, words: list[str]) -> dict:
        result = {}
        for idx, word in enumerate(words):
            dict_element = result.get(word)
            if dict_element:
                dict_element.append(idx)
            else:
                result[word] = [0, 2, idx]
        return result

    def encode_word(self, s: str, word_len: int, dictionary: dict) -> list[int]:
        result = []
        str_lenght = len(s)
        start = 0
        while start <= str_lenght:
            sub_word = s[start:start+word_len]
            if sub_word in dictionary:
                dict_elm = dictionary[sub_word]
                result.append(dict_elm.pop())
                if len(dict_elm) == 0:
                    dictionary.pop(sub_word)
            start += word_len
        print(result)
        return result

    def permutate_word(self, word: str, sub_word_len: int, dictionary: dict) -> bool:
        result = []
#        str_lenght = len(word)
        start = 0
        while start < len(word):
            sub_word = word[start:start+sub_word_len]
            if sub_word in dictionary:
                dict_elm = dictionary[sub_word]
                if dict_elm:
                    result.append(dict_elm.pop())
                    print(result)
                else:
                    return False
#                if len(dict_elm) == 0:
#                    dictionary.pop(sub_word)
            else:
                return False
            start += sub_word_len
#        print(result)
        return True

    def permutate_word2(self, word: str, sub_word_len: int, dictionary: dict, itter_no: int) -> bool:
        result = []
#        str_lenght = len(word)
        start = 0
        while start < len(word):
            sub_word = word[start:start+sub_word_len]
            dict_elm = dictionary.get(sub_word)
            if dict_elm:
                if dict_elm[0] == itter_no:
                    if dict_elm[1] > (len(dict_elm) - 1):
                        return False
                    else:
                        result.append(dict_elm[dict_elm[1]])
                        dict_elm[1] += 1

                else:
                    dict_elm[0] = itter_no
                    dict_elm[1] = 3
                    result.append(dict_elm[2])
#                    print(result)

            #                if len(dict_elm) == 0:
            #                    dictionary.pop(sub_word)
            else:
                return False
            start += sub_word_len
        #        print(result)
        return True


    def findSubstring(self, s: str, words: list[str]) -> list[int]:

        dict_of_words = self.dict_of_words(self, words)
#       self.encode_word(self, s, len(words[0]), dict_of_words)
        word_len = len(words[0])
        permutation_len = word_len * len(words)
#        self.permutate_word(self, s[0:permutation_len], word_len, dict_of_words)
        end = len(s)-permutation_len
        index = 0
        result = []
        while index <= end:
            if self.permutate_word2(self, s[index:index+permutation_len], word_len, dict_of_words, index):
                result.append(index)
            index += 1
        return result





res = Solution
print(res.findSubstring(res,"lingmindraboofooowingdingbarrwingmonkeypoundcake",["fooo","barr","wing","ding","wing"]))

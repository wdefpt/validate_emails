'''
You are given an array of integers distance.

You start at point (0,0) on an X-Y plane and you move distance[0] meters to the north, then distance[1] meters to the west,
distance[2] meters to the south, distance[3] meters to the east, and so on. In other words, after each move, your direction changes counter-clockwise.

Return true if your path crosses itself, and false if it does not.


Constraints:

1 <= distance.length <= 10 ** 5
1 <= distance[i] <= 10 ** 5

'''
class Solution:
    def isSelfCrossing(self, distance: list[int]) -> bool:
        increasing = True
        rubikon = 0
        for idx, dst in enumerate(distance):
            if increasing:
                if idx > 1 and dst <= distance[idx - 2]:
                    print(idx)
                    rubikon = idx
                    increasing = False
                    if idx > 3 and dst >= distance[idx - 2] - distance[idx - 4]:
                        limit1 = distance[idx - 1] - distance[idx - 3]
                    elif idx == 3 and dst == distance[idx - 2]:
                        limit1 = distance[idx - 1] - distance[idx - 3]
                    else:
                        limit1 = 10 ** 5 + 1
            else:
                if idx == rubikon + 1 and dst >= limit1:
                    return True
                if dst >= distance[idx - 2]:
                    return True
        return False








#array = [[False for j in range(5)] for i in range(5)]

#i = 4

#result = distance[i] > distance[i - 2] or

#print(array)

#print(array)

sol = Solution
print(sol.isSelfCrossing(sol, [3, 3, 4, 2, 2]))
print(sol.isSelfCrossing(sol, [1, 1, 2, 1, 1]))
print(sol.isSelfCrossing(sol, [3, 3, 5, 3, 2]))
print(sol.isSelfCrossing(sol, [2, 1, 1, 2]))
print(sol.isSelfCrossing(sol, [1, 1, 2, 2, 1, 1]))
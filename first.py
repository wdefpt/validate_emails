# Write function to validate email addresses. The requirements are the following:

# * It must have the username@websitename.extension format type.
# * The username can only contain letters, digits, dashes and underscores .
# * The website name can only have letters and digits
# * The extension can only contain letters
# * The maximum length of the extension is 3
# * Do not use regular expressions

# Also implement tests to verify its behavior
"""

from typing import List

def validate_emails(emails: List[str]) -> bool:
    pass
"""


def validate_one_email(email: str) -> bool:
    if not type(email) is str:
        return False
    email.casefold()
    temp_list = email.rsplit('@')
    if len(temp_list) == 2:
        username = temp_list[0]
        remains = temp_list[1]
        table = username.maketrans('-_', 'oo')
        username = username.translate(table)
        temp_list = remains.rsplit('.')
        if len(temp_list) == 2:
            domain2 = temp_list[0]
            table = domain2.maketrans('-', 'o')
            domain2 = domain2.translate(table)
            domain1 = temp_list[1]
            #            print(username + '\n' + domain2 + '\n' + domain1)
            return domain1.isalpha() and domain2.isalnum() and username.isalnum() and 0 < len(domain1) < 4
        else:
            return False
    else:
        return False


def validate_emails(emails: list[str]) -> bool:
    if not emails:
        raise Exception('"emails" parameter can''t be empty')
    for email in emails:
        if not validate_one_email(email):
            return False
    return True


# Unit tests

def test_validate_emails() -> bool:
    true_list = ['vano127@yandex.ru', 'VAno127@gmail.com', '1234234@yasj-whf.er', '_qwerqw-er@afasdwef.sdf']
    false_list = ['vano127@yandex.ru', 'VAno127@gmail.com', '1234234@yasj-whf.er', '_qwerqw-er@afasdwef.sdf',
                  'qweqwedf@sdasd.eqwes']
    return validate_emails(true_list) and not validate_emails(false_list)


def test_validate_one_email() -> bool:
    true_list = ['vano127@yandex.ru', 'VAno127@gmail.com', '12ert34234@yasj-whf.er', '_qwerqw-er@afasdwef.sdf']
    false_list = [0, '', ' ', 'asdfasdf', 'asdfasfdasdf.werwsdf', 'asdf&^@asdf.df', 'asdf@asdf#.ur',
                  'qweqwedf@sdasd.eqwes', 'vano127@yandex.ru1']
    overall_result = True
    for email in true_list:
        result = validate_one_email(email)
        overall_result = overall_result and result
        print('Example true_list "{}" - {}'.format(email, result))
    for email in false_list:
        result = validate_one_email(email)
        overall_result = overall_result and not result
        print('Example false_list "{}" - {}'.format(email, result))
    return overall_result


if test_validate_one_email():
    print('Tests for "validate_one_email" are completed without errors.')
else:
    print('Tests for "validate_ine_email" found some errors!')

if test_validate_emails():
    print('Tests "test_validate_emails" are completed without errors.')
else:
    print('Tests "test_validate_emails" found some errors!')

class Solution:
    def findSubstring(self, s: str, words: list[str]) -> list[int]:
        if not s or not words or not words[0]:
            return []
        len_s = len(s)
        len_word = len(words[0])
        len_permutate = len(words) * len_word
        word_dict = {}
        for w in words:
            word_dict[w] = word_dict[w] + 1 if w in word_dict else 1
        ans = []
        for i in range(min(len_word, len_s - len_permutate + 1)):
            self._findSubstring(self, i, i, len_s, len_word, len_permutate, s, word_dict, ans)
        return ans

    def _findSubstring(self, idx1, idx2, len_s, len_word, len_permutate, s, word_dict, ans):
        curr = {}
        while idx2 + len_word <= len_s:
            w = s[idx2:idx2 + len_word]
            idx2 += len_word
            if w not in word_dict:
                idx1 = idx2
                curr.clear()
            else:
                curr[w] = curr[w] + 1 if w in curr else 1
                while curr[w] > word_dict[w]:
                    curr[s[idx1:idx1 + len_word]] -= 1
                    idx1 += len_word
                if idx2 - idx1 == len_permutate:
                    ans.append(idx1)



res = Solution


print(res.findSubstring(res,"lingmindraboofooowingdingbarrwingmonkeypoundcake",["fooo","barr","wing","ding","wing"]))
